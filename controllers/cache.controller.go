package controllers

import (
	"net/http"
  
	"github.com/gin-gonic/gin"
	"github.com/kre8mymedia/gingorm/services"
)

type CacheInput struct {
	Key   string `json:"key" binding:"required"`
	Value interface{} `json:"value"`
}

func CacheGet(ctx *gin.Context) {
	var input CacheInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	payload, err := services.RedisGet(input.Key)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"success": false})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"success": true,
		"value": payload,
	})
}

func CacheSet(ctx *gin.Context) {
	var input CacheInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	payload := services.RedisSet(input.Key, input.Value)
	if payload != true {
		ctx.JSON(http.StatusOK, gin.H{"success": false})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"success": payload})
}