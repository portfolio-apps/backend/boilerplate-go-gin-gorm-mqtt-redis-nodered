package controllers

import (
	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
	"github.com/kre8mymedia/gingorm/auth"
	"github.com/kre8mymedia/gingorm/models"
)

func FindOrganizations(c *gin.Context) {
  var organizations []models.Organization
  models.DB.Find(&organizations)

  c.JSON(http.StatusOK, gin.H{
	"success": true,
	"organizations": organizations,
  })
}

func CreateOrganization(c *gin.Context) {
	// Validate input
	var input models.CreateOrganizationInput
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}

	token, err := auth.ExtractClaims(c.Request.Header["Authorization"][0])
	if err != false {
		fmt.Print(err)
	}

	var id uint = uint(token["id"].(float64))
  
	// Create organization
	organization := models.Organization{
		PrimaryUserID: id,
		Name: input.Name,
		Slug: input.Slug,
	}
	models.DB.Create(&organization)

	var user models.User
	if err := models.DB.Where("id = ?", id).First(&user).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}

	models.DB.Model(&user).Update("OrganizationID", organization.ID)
  
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"organization": organization,
	})
}

// GET /organizations/:id
// Find a book
func FindOrganization(c *gin.Context) {  // Get model if exist
	var organization models.Organization
	if err := models.DB.Preload("Users").Where("id = ?", c.Param("id")).First(&organization).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}

	token, err := auth.ExtractClaims(c.Request.Header["Authorization"][0])
	if err != false {
		fmt.Print(err)
	}
	// Get AuthUserId
	var id uint = uint(token["id"].(float64))
	// Retrieve User
	var user models.User
	if err := models.DB.Where("id = ?", id).First(&user).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
	// Check authorization
	if user.OrganizationID != organization.ID {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		return
	}
  
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"organization": organization,
	})
}