package controllers

import (
	"net/http"
  
	"github.com/gin-gonic/gin"
	services "github.com/kre8mymedia/gingorm/services"
)

type MqttInput struct {
	Topic   string `json:"topic" binding:"required"`
	Payload string `json:"payload" binding:"required"`
	Count   int    `json:"count"`
}

func BrokerPublish(ctx *gin.Context) {
	// request
	var input MqttInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// action
	payload, err := services.Publish(input.Topic, input.Payload)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"published": false})
	}
	// response
	ctx.JSON(http.StatusOK, gin.H{"published": payload})
}

func BrokerThreadedPublish(ctx *gin.Context) {
	// request
	var input MqttInput
	if err := ctx.ShouldBindJSON(&input); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// action
	payload, err := services.ThreadedPublish(input.Count, input.Topic, input.Payload)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{"published": false})
	}
	// response
	ctx.JSON(http.StatusOK, gin.H{"published": payload})
}