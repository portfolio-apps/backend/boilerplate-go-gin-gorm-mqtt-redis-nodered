package controllers

import (
  "net/http"

  "github.com/gin-gonic/gin"
  "github.com/kre8mymedia/gingorm/models"
)



// PingExample godoc
// @Summary List of books
// @Schemes
// @Description Returns a list of books
// @Tags Book
// @Accept json
// @Produce json
// @Success 200 {string} Helloworld
// @Router /books [get]
func FindBooks(c *gin.Context) {
  var books []models.Book
  models.DB.Find(&books)

  c.JSON(http.StatusOK, gin.H{"books": books})
}

// POST /books
// Create new book
func CreateBook(c *gin.Context) {
	// Validate input
	var input models.CreateBookInput
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}
  
	// Create book
	book := models.Book{Title: input.Title, Author: input.Author}
	models.DB.Create(&book)
  
	c.JSON(http.StatusOK, gin.H{"book": book})
}

// GET /books/:id
// Find a book
func FindBook(c *gin.Context) {  // Get model if exist
	var book models.Book
  
	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
  
	c.JSON(http.StatusOK, gin.H{"book": book})
}

// PATCH /books/:id
// Update a book
func UpdateBook(c *gin.Context) {
	// Get model if exist
	var book models.Book
	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
  
	// Validate input
	var input models.UpdateBookInput
	if err := c.ShouldBindJSON(&input); err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	  return
	}
  
	models.DB.Model(&book).Updates(input)
  
	c.JSON(http.StatusOK, gin.H{"book": book})
}

// DELETE /books/:id
// Delete a book
func DeleteBook(c *gin.Context) {
	// Get model if exist
	var book models.Book
	if err := models.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
	  c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
	  return
	}
  
	models.DB.Delete(&book)
  
	c.JSON(http.StatusOK, gin.H{"success": true})
}