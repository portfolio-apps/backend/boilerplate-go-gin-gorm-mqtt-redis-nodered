-- Create Databases
CREATE DATABASE IF NOT EXISTS `gingorm`;
CREATE DATABASE IF NOT EXISTS `gingorm_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'gingorm'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;
