package services

import (
	"os"
	"testing"
	"github.com/kre8mymedia/gingorm/entities"
)

func TestPublish(t *testing.T){
	os.Setenv("MQTT_HOST", "192.168.1.117")
	os.Setenv("MQTT_PORT", "1883")
	os.Setenv("MQTT_USERNAME", "username")
	os.Setenv("MQTT_PASSWORD", "password")
	
    published, err := Publish(entities.Topic["test"], "Successful test")

    if err != nil {
		println("ERROR")
	}
	println(published)
}

func TestThreadedPublish(t *testing.T){
	os.Setenv("MQTT_HOST", "192.168.1.117")
	os.Setenv("MQTT_PORT", "1883")
	os.Setenv("MQTT_USERNAME", "username")
	os.Setenv("MQTT_PASSWORD", "password")
	
    published, err := ThreadedPublish(100, entities.Topic["test"], "Successful test")

    if err != nil {
		println("ERROR")
	}
	println(published)
}