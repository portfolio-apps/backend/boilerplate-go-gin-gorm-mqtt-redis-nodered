#!/bin/bash

#############################################################
### TO INSTALL (For Production would resort to RDMS on AWS)
#############################################################
### helm -n $1 repo add stable https://charts.helm.sh/stable
### helm -n $1 install my-release bitnami/phpmyadmin
#############################################################
### When DB up, login as root and create an application user. 
### DO NOT USE root as db user for application
#############################################################
### 
### New User
### CREATE USER 'dbuser'@'localhost' IDENTIFIED BY 'complexPassword';
###
### Grant Privileges to specific db
### GRANT ALL PRIVILEGES ON db_name . * TO 'dbuser'@'localhost';
###
### Reload the privileges
### FLUSH PRIVILEGES;
##########################################################

echo "=================================="
echo " Installing MySQL..."
echo "=================================="
helm -n $1 install mysql stable/mysql
echo 
echo

echo " MySQL Password"
echo "----------------------------------"
export MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace $1 mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)
echo $MYSQL_ROOT_PASSWORD
echo
echo

echo "=================================="
echo " PHPMyAdmin Installing..."
echo "=================================="
helm -n $1 install phpmyadmin --set db.host=mysql,db.port=3306 bitnami/phpmyadmin
echo
echo

echo "=================================="
echo " Resources in [$1] Namespace..."
echo "=================================="
kubectl -n $1 get all
echo
echo
