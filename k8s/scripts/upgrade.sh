#!/bin/bash

### Set Environment Variables
set -a # automatically export all variables
source .env.production
set +a

helm -n $1 upgrade -i --debug --wait --atomic $2 \
--set dbPassword=$DB_PASSWORD \
--set mqttPassword=$MQTT_PASSWORD \
--set redisHost=$REDIS_HOST \
--set redisPort=$REDIS_PORT \
--set redisPassword=$REDIS_PASSWORD \
./k8s/helm

echo
echo

kubectl -n $1 get all