#!/bin/sh

docker stop golang-rdb
docker system prune -a
docker run --name golang-rdb -d -p 6379:6379 redis:6
docker exec -it golang-rdb redis-cli
