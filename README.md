# Golang Gin Gorm Boilerplate Project

## Table of Contents
- [HTTP Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)

### Build Docker Network (Gin & Gorm, MySQL, PHPMyAdmin, Redis, RedisUi, MQTT)
<pre>
docker-compose up --build
</pre>

### Run Node-RED
<pre>
bash scripts/local_node_red.sh
</pre>

### Run Tests
<pre>
<b>Unit Tests</b>
bash scripts/test_unit.sh

<b>Integration Tests</b>
bash scripts/test.sh
</pre>