package main

import (
	"github.com/gin-gonic/gin"

	docs "github.com/kre8mymedia/gingorm/docs"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/kre8mymedia/gingorm/controllers"
	"github.com/kre8mymedia/gingorm/middlewares"
	"github.com/kre8mymedia/gingorm/models"
)

func initRouter() *gin.Engine {
	r := gin.Default()
	// @BasePath /api/v1
	docs.SwaggerInfo.BasePath = "/api/v1"
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	/** ---------------------------------------------------------------------------------------
	*	Static Routes
	* -------------------------------------------------------------------------------------------
	 */
	r.LoadHTMLGlob("templates/*.html")
	r.GET("/", func(ctx *gin.Context) {
		ctx.HTML(200, "index.html", gin.H{})
	})

	/** ---------------------------------------------------------------------------------------
	*	Un-secured API Routes
	* -------------------------------------------------------------------------------------------
	 */
	api := r.Group("/api/v1")
	{
		// Auth Routes
		api.POST("/token", controllers.GenerateToken)
		api.POST("/user/register", controllers.RegisterUser)

		// Book Routes
		api.GET("/books", controllers.FindBooks)
		api.POST("/books", controllers.CreateBook)
		api.GET("/books/:id", controllers.FindBook)
		api.PATCH("/books/:id", controllers.UpdateBook)
		api.DELETE("/books/:id", controllers.DeleteBook)

		// Cache Routes
		api.GET("/cache", controllers.CacheGet)
		api.POST("/cache", controllers.CacheSet)

		// Broker Routes
		api.POST("/pub", controllers.BrokerPublish)
		api.POST("/threaded/pub", controllers.BrokerThreadedPublish)
	}

	/** ---------------------------------------------------------------------------------------
	*	Secured API Routes
	* -------------------------------------------------------------------------------------------
	 */
	secured := r.Group("/api/v1").Use(middlewares.Auth())
	{
		// Test Auth Route
		secured.GET("/ping", controllers.Ping)

		// Organization Secured Routes
		secured.GET("/organizations", controllers.FindOrganizations)
		secured.POST("/organizations", controllers.CreateOrganization)
		secured.GET("/organizations/:id", controllers.FindOrganization)
	}

	return r
}

func main() {
	models.ConnectDatabase()
	r := initRouter()
	r.Run(":8080")
}
