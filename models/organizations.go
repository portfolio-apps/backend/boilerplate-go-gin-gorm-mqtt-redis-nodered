package models

import "time"

type Organization struct {
	ID            uint       `json:"id" gorm:"primary_key"`
	CreatedAt     time.Time  `json:"createdAt"`
	UpdatedAt     time.Time  `json:"-"`
	DeletedAt     *time.Time `json:"-"`
	PrimaryUserID uint       `json:"primaryUserId" gorm:"unique"`
	Name          string     `json:"name"`
	Slug          string     `json:"slug" gorm:"unique"`
	Users         []User     `json:"users,omitempty"`
}

type CreateOrganizationInput struct {
	Name string `json:"name" binding:"required"`
	Slug string `json:"slug" binding:"required"`
}
