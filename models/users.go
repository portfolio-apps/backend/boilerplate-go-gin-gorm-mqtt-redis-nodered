package models

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID             uint         `json:"id" gorm:"primary_key"`
	Name           string       `json:"name" binding:"required"`
	Username       string       `json:"username" binding:"required" gorm:"unique"`
	Email          string       `json:"email" binding:"required" gorm:"unique"`
	Password       string       `json:"password" binding:"required,min=8"`
	OrganizationID uint         `json:"-"`
	Organization   Organization `json:"-"`
	CreatedAt      time.Time    `json:"-"`
	UpdatedAt      time.Time    `json:"-"`
	DeletedAt      *time.Time   `json:"-"`
}

type CreateUserInput struct {
	Name     string `json:"name" binding:"required"`
	Username string `json:"username" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (user *User) HashPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return err
	}
	user.Password = string(bytes)
	return nil
}
func (user *User) CheckPassword(providedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(providedPassword))
	if err != nil {
		return err
	}
	return nil
}
